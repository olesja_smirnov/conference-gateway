package com.conferencegateway.service;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.Participant;
import com.conferencegateway.domain.repo.ParticipantRepository;
import com.conferencegateway.errors.ResourceNotFoundException;
import com.conferencegateway.web.request.ParticipantRequest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.time.LocalDate;
import java.util.Optional;

import static com.conferencegateway.errors.ErrorMsg.NOT_FOUND_PARTICIPANT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantServiceTest {

    private static final LocalDate BIRTHDAY = LocalDate.of(2000, 1, 1);
    private static final Long PARTICIPANT_ID = 2L;
    private static final String PARTICIPANT_NAME = "Billi";

    private static Conference CONFERENCE = Conference.builder().build();

    private static Participant PARTICIPANT = Participant.builder()
            .id(PARTICIPANT_ID)
            .name(PARTICIPANT_NAME)
            .birthday(BIRTHDAY)
            .build();

    private static ParticipantRequest PARTICIPANT_REQUEST = ParticipantRequest.builder()
            .name(PARTICIPANT_NAME)
            .birthday(BIRTHDAY)
            .build();

    @InjectMocks
    private ParticipantService participantService;

    @Mock
    private ParticipantRepository participantRepository;

    @Rule //initMocks
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldReturnExistingOwner() {
        when(participantRepository.findByNameAndBirthday(any(), any())).thenReturn(Optional.of(PARTICIPANT));

        Long response = participantService.getExistingOrNewOwnerId(PARTICIPANT_REQUEST);

        assertNotNull(response);
        assertEquals(response, PARTICIPANT_ID);
    }

    @Test
    public void shouldCreateNewOwner() {
        when(participantRepository.findByNameAndBirthday(any(), any())).thenReturn(Optional.empty());
        when(participantRepository.save(any())).thenReturn(PARTICIPANT);

        Long response = participantService.getExistingOrNewOwnerId(PARTICIPANT_REQUEST);

        assertNotNull(response);
        assertEquals(response, PARTICIPANT_ID);
    }

    @Test
    public void shouldCreateNewParticipant() {
        PARTICIPANT.setConference(CONFERENCE);
        when(participantRepository.save(any())).thenReturn(PARTICIPANT);

        Participant response = participantService.createNewParticipant(PARTICIPANT_REQUEST, CONFERENCE);

        assertNotNull(response);
        assertEquals(response.getName(), PARTICIPANT_NAME);
        assertEquals(response.getBirthday(), BIRTHDAY);
        assertEquals(response.getConference(), CONFERENCE);
    }

    @Test
    public void shouldFindParticipant() {
        when(participantRepository.findByIdAndConference(any(), any())).thenReturn(Optional.of(PARTICIPANT));

        Participant response = participantService.getParticipant(PARTICIPANT_ID, CONFERENCE);

        assertNotNull(response);
        assertEquals(response, PARTICIPANT);
    }

    @Test
    public void shouldThrowExceptionWhileSearchingParticipant() {
        when(participantRepository.findByIdAndConference(any(), any())).thenThrow(new ResourceNotFoundException(NOT_FOUND_PARTICIPANT));
        thrown.expect(ResourceNotFoundException.class);
        thrown.expectMessage(NOT_FOUND_PARTICIPANT);

        participantService.getParticipant(PARTICIPANT_ID, CONFERENCE);
    }

    @Test
    public void shouldRemoveParticipant() {
        participantService.removeParticipant(PARTICIPANT);

        verify(participantRepository, times(1)).delete(PARTICIPANT);
    }

}