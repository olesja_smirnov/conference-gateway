package com.conferencegateway.service;

import com.conferencegateway.domain.model.ConferenceRoom;
import com.conferencegateway.domain.repo.ConferenceRoomRepository;
import com.conferencegateway.web.request.RoomRequest;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConferenceRoomServiceTest {

    private static final LocalDate CONFERENCE_DATE = LocalDate.now();
    private static final Long REGISTERED_PARTICIPANTS = 5L;
    private static final Long ROOM_SIZE = 10L;
    private static final ConferenceRoom ROOM = ConferenceRoom.builder()
            .roomSize(ROOM_SIZE)
            .build();

    private static final RoomRequest ROOM_REQUEST = RoomRequest.builder()
            .registeredParticipants(REGISTERED_PARTICIPANTS)
            .conferenceDate(CONFERENCE_DATE)
            .build();

    @InjectMocks
    private ConferenceRoomService roomService;

    @Mock
    private ConferenceRoomRepository roomRepository;

    @Mock
    private ConferenceService conferenceService;

    @Test
    public void shouldReturnAllAvailableRooms() {
        when(conferenceService.findBookedConference(CONFERENCE_DATE)).thenReturn(Collections.emptyList());
        when(roomRepository.findByRoomSizeGreaterThanEqual(REGISTERED_PARTICIPANTS)).thenReturn(ImmutableList.of(ROOM));

        List<ConferenceRoom> response = roomService.getAvailableRooms(ROOM_REQUEST);

        assertNotNull(response);
        assertThat(response, hasSize(is(1)));

        ConferenceRoom room = response.get(0);

        assertEquals(room.getRoomSize(), ROOM_SIZE);
    }

}