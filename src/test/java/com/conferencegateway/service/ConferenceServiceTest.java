package com.conferencegateway.service;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.Participant;
import com.conferencegateway.domain.repo.ConferenceRepository;
import com.conferencegateway.domain.repo.ParticipantRepository;
import com.conferencegateway.errors.ResourceNotFoundException;
import com.conferencegateway.web.request.ConferenceRequest;
import com.conferencegateway.web.request.ConferenceRoomRequest;
import com.conferencegateway.web.request.ParticipantRequest;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static com.conferencegateway.domain.enums.ConferenceStatus.CANCELLED;
import static com.conferencegateway.errors.ErrorMsg.NOT_FOUND_CONFERENCE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConferenceServiceTest {

    private static final LocalDate BIRTHDAY = LocalDate.of(2000, 1, 1);
    private static final LocalDate BIRTHDAY_2 = LocalDate.of(1900, 10, 27);
    private static final Long CONFERENCE_ID = 1L;
    private static final LocalDate CONFERENCE_DATE = LocalDate.of(2021, 10, 30);
    private static final LocalTime CONFERENCE_TIME = LocalTime.of(10, 0, 0);
    private static final String LOCATION = "Tallinn";
    private static final Long OWNER_ID = 23L;
    private static final Long OWNER_ID_2 = 33L;
    private static final String OWNER_NAME = "Olesja";
    private static final String OWNER_NAME_2 = "John";
    private static final Long PARTICIPANT_ID = 2L;
    private static final Long PARTICIPANT_ID_2 = 33L;
    private static final String ROOM_NAME = "Venus";
    private static final Long ROOM_SIZE = 10L;
    private static final String TITLE = "Java conference";

    private static final Participant OWNER = Participant.builder()
            .name(OWNER_NAME)
            .birthday(BIRTHDAY)
            .build();

    private static final ParticipantRequest OWNER_REQUEST = ParticipantRequest.builder()
            .name(OWNER_NAME)
            .birthday(BIRTHDAY)
            .build();

    private static Conference CONFERENCE = Conference.builder()
            .id(CONFERENCE_ID)
            .ownerId(OWNER_ID)
            .title(TITLE)
            .date(CONFERENCE_DATE)
            .startTime(CONFERENCE_TIME)
            .build();

    private final static ConferenceRequest CONFERENCE_REQUEST = ConferenceRequest.builder()
            .owner(OWNER_REQUEST)
            .title(TITLE)
            .date(CONFERENCE_DATE)
            .startTime(CONFERENCE_TIME)
            .build();

    private static Participant PARTICIPANT = Participant.builder()
            .id(PARTICIPANT_ID)
            .name(OWNER_NAME_2)
            .birthday(BIRTHDAY_2)
            .build();

    private static ParticipantRequest PARTICIPANT_REQUEST = ParticipantRequest.builder()
            .name(OWNER_NAME_2)
            .birthday(BIRTHDAY_2)
            .build();

    private static final ConferenceRoomRequest ROOM_REQUEST = ConferenceRoomRequest.builder()
            .roomName(ROOM_NAME)
            .location(LOCATION)
            .roomSize(ROOM_SIZE)
            .build();

    @InjectMocks
    private ConferenceService conferenceService;

    @Mock
    private ConferenceRepository conferenceRepository;

    @Mock
    private ParticipantService participantService;

    @Mock
    private ParticipantRepository participantRepository;

    @Mock
    private ConferenceRoomService conferenceRoomService;

    @Rule //initMocks
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeEach
    public void setup() {
        CONFERENCE.setParticipants(null);
        PARTICIPANT_REQUEST = ParticipantRequest.builder()
                .name(OWNER_NAME_2)
                .birthday(BIRTHDAY_2)
                .build();
    }

    @Test
    public void shouldCreateNewConference() {
        CONFERENCE.setOwnerId(OWNER_ID);
        when(participantService.getExistingOrNewOwnerId(any())).thenReturn(OWNER_ID);
        when(conferenceRepository.save(any())).thenReturn(CONFERENCE);

        Conference response = conferenceService.createNewConference(CONFERENCE_REQUEST);

        assertNotNull(response);
        assertEquals(response.getOwnerId(), OWNER_ID);
        assertEquals(response.getTitle(), TITLE);
    }

    @Test
    public void shouldReturnAllConferences() {
        when(conferenceRepository.findAll()).thenReturn(ImmutableList.of(CONFERENCE));

        List<Conference> response = conferenceService.getConferences();

        assertNotNull(response);
        assertThat(response, hasSize(is(1)));

        Conference conference = response.get(0);

        assertEquals(conference.getOwnerId(), OWNER_ID);
        assertEquals(conference.getTitle(), TITLE);
    }

    @Test
    public void shouldCancelConference() {
        when(conferenceRepository.findById(any())).thenReturn(Optional.of(CONFERENCE));
        when(conferenceRepository.save(any())).thenReturn(CONFERENCE);

        Conference response = conferenceService.cancelConference(CONFERENCE_ID);

        assertNotNull(response);
        assertEquals(response.getStatus(), CANCELLED);
    }

    @Test
    public void shouldChangeConferenceOwner() {
        when(conferenceRepository.findById(any())).thenReturn(Optional.of(CONFERENCE));
        when(participantService.getExistingOrNewOwnerId(any())).thenReturn(PARTICIPANT_ID_2);
        when(conferenceRepository.save(any())).thenReturn(CONFERENCE);

        Conference response = conferenceService.changeConferenceOwner(CONFERENCE_ID, PARTICIPANT_REQUEST);

        assertNotNull(response);
        assertEquals(response.getOwnerId(), OWNER_ID_2);
    }

    @Test
    public void shouldAddParticipantToConference() {
        when(conferenceRepository.findById(any())).thenReturn(Optional.of(CONFERENCE));
        when(participantService.createNewParticipant(any(), any())).thenReturn(PARTICIPANT);

        Conference response = conferenceService.addNewParticipantToConference(CONFERENCE_ID, PARTICIPANT_REQUEST);

        assertNotNull(response);
        assertTrue(response.getParticipants().contains(PARTICIPANT));
    }

    @Test
    public void shouldReturnConferenceById() {
        when(conferenceRepository.findById(any())).thenReturn(Optional.of(CONFERENCE));

        Conference response = conferenceService.getConferenceById(CONFERENCE_ID);

        assertNotNull(response);
        assertEquals(response, CONFERENCE);
    }

    @Test
    public void shouldThrowExceptionWhileSearchingConferenceById() {
        when(conferenceRepository.findById(any())).thenThrow(new ResourceNotFoundException(NOT_FOUND_CONFERENCE));
        thrown.expect(ResourceNotFoundException.class);
        thrown.expectMessage(NOT_FOUND_CONFERENCE);

        conferenceService.getConferenceById(CONFERENCE_ID);
    }

    @Test
    public void shouldRemoveParticipantFromConference() {
        CONFERENCE = Conference.builder()
                .id(CONFERENCE_ID)
                .ownerId(OWNER_ID)
                .title(TITLE)
                .date(CONFERENCE_DATE)
                .startTime(CONFERENCE_TIME)
                .build();
        PARTICIPANT = Participant.builder()
                .id(PARTICIPANT_ID)
                .name(OWNER_NAME_2)
                .birthday(BIRTHDAY_2)
                .build();

        when(conferenceRepository.findById(any())).thenReturn(Optional.of(CONFERENCE));
        Conference response = conferenceService.removeParticipantFromConference(CONFERENCE_ID, PARTICIPANT_ID);

        assertNotNull(response);
        assertEquals(response, CONFERENCE);
        assertFalse(response.getParticipants().contains(PARTICIPANT));
    }

    @Test
    public void shouldAddRoomToConference() {
        when(conferenceRepository.findById(any())).thenReturn(Optional.of(CONFERENCE));
        when(conferenceRepository.save(any())).thenReturn(CONFERENCE);

        Conference response = conferenceService.addRoomToConference(CONFERENCE_ID, ROOM_REQUEST);

        assertNotNull(response);
        assertEquals(response, CONFERENCE);
        assertEquals(response.getRoom().getRoomName(), ROOM_NAME);
        assertEquals(response.getRoom().getRoomSize(), ROOM_SIZE);
    }

}