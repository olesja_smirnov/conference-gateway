package com.conferencegateway.web.response;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceResponse {

    @NotNull
    private Long id;

    @NotNull
    private ParticipantResponse owner;

    @NotNull
    private String title;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime startTime;

    @NotNull
    private String status;

    private ConferenceRoomResponse room;

    private Set<ParticipantResponse> participants;

}