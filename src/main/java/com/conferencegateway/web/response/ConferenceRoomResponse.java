package com.conferencegateway.web.response;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceRoomResponse {

    @NotNull
    private Long id;

    @NotNull
    private String roomName;

    @NotNull
    private String location;

    @NotNull
    private Long roomSize;

}