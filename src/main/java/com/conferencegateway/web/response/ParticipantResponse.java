package com.conferencegateway.web.response;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParticipantResponse {

    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private LocalDate birthday;

}