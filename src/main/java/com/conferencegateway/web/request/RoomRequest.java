package com.conferencegateway.web.request;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.sun.istack.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoomRequest {

    @NotNull
    private Long registeredParticipants;

    @NotNull
    private LocalDate conferenceDate;

}