package com.conferencegateway.web.request;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConferenceRoomRequest {

    @NotNull
    private Long id;

    @NotNull
    private String roomName;

    @NotNull
    private String location;

    @NotNull
    private Long roomSize;

}