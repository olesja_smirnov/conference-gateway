package com.conferencegateway.web.request;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConferenceRequest {

    @NotNull
    private ParticipantRequest owner;

    @NotNull
    private String title;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime startTime;

}