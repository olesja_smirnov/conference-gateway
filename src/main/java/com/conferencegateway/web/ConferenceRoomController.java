package com.conferencegateway.web;

import com.conferencegateway.domain.model.ConferenceRoom;
import com.conferencegateway.mapper.ConferenceRoomMapper;
import com.conferencegateway.service.ConferenceRoomService;
import com.conferencegateway.web.request.RoomRequest;
import com.conferencegateway.web.response.ConferenceRoomResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/room")
@Api(tags = "room", value = "/api/room")
public class ConferenceRoomController {

    @Autowired
    private ConferenceRoomService conferenceRoomService;

    @Autowired
    private ConferenceRoomMapper conferenceRoomMapper;

    @ApiOperation("Get available Conference Rooms")
    @PostMapping
    public List<ConferenceRoomResponse> getConferenceRooms(@RequestBody RoomRequest request) {
        List<ConferenceRoom> conferences = conferenceRoomService.getAvailableRooms(request);

        return conferences.stream().map(conferenceRoomMapper::convertTo).collect(Collectors.toList());
    }

}