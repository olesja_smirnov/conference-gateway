package com.conferencegateway.web;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.mapper.ConferenceMapper;
import com.conferencegateway.service.ConferenceService;
import com.conferencegateway.web.request.ConferenceRequest;
import com.conferencegateway.web.request.ConferenceRoomRequest;
import com.conferencegateway.web.request.ParticipantRequest;
import com.conferencegateway.web.response.ConferenceResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/conference")
@Api(tags = "conference", value = "/api/conference")
public class ConferenceController {

    @Autowired
    private ConferenceService conferenceService;

    @Autowired
    private ConferenceMapper conferenceMapper;

    @ApiOperation("Create new Conference")
    @PostMapping
    public ConferenceResponse createNewConference(@RequestBody ConferenceRequest request) {
        Conference newConference = conferenceService.createNewConference(request);

        return conferenceMapper.convertTo(newConference);
    }

    @ApiOperation("Get all Conferences")
    @GetMapping
    public List<ConferenceResponse> getConferences() {
        List<Conference> conferences = conferenceService.getConferences();

        return conferences.stream().map(conferenceMapper::convertTo).collect(Collectors.toList());
    }

    @ApiOperation("Get Conference by ID")
    @GetMapping("/{id}")
    public ConferenceResponse getConferenceById(@PathVariable Long id) {
        Conference conference = conferenceService.getConferenceById(id);

        return conferenceMapper.convertTo(conference);
    }

    @ApiOperation("Update Conference")
    @PutMapping("/{id}")
    public ConferenceResponse updateConference(@PathVariable Long id, @RequestBody ConferenceRequest request) {
        Conference updatedConference = conferenceService.updateConference(id, request);

        return conferenceMapper.convertTo(updatedConference);
    }

    @ApiOperation("Add new Participant to Conference")
    @PostMapping("/{id}/participant")
    public ConferenceResponse addNewParticipantToConference(@PathVariable Long id, @RequestBody ParticipantRequest request) {
        Conference newConference = conferenceService.addNewParticipantToConference(id, request);

        return conferenceMapper.convertTo(newConference);
    }

    @ApiOperation("Cancel Conference")
    @PatchMapping("/{id}/cancel")
    public ConferenceResponse cancelConference(@PathVariable Long id) {
        Conference cancelledConference = conferenceService.cancelConference(id);

        return conferenceMapper.convertTo(cancelledConference);
    }

    @ApiOperation("Remove Participant from Conference")
    @DeleteMapping("/{conferenceId}/participant/{participantId}/remove")
    public ConferenceResponse removeParticipantFromConference(@PathVariable Long conferenceId, @PathVariable Long participantId) {
        Conference conference = conferenceService.removeParticipantFromConference(conferenceId, participantId);

        return conferenceMapper.convertTo(conference);
    }

    @ApiOperation("Change Conference Owner")
    @PatchMapping("/{conferenceId}/owner")
    public ConferenceResponse changeConferenceOwner(@PathVariable Long conferenceId, @RequestBody ParticipantRequest request) {
        Conference conference = conferenceService.changeConferenceOwner(conferenceId, request);

        return conferenceMapper.convertTo(conference);
    }

    @ApiOperation("Add Room to Conference")
    @PatchMapping("/{id}/room")
    public ConferenceResponse addRoomToConference(@PathVariable Long id, @RequestBody ConferenceRoomRequest request) {
        Conference cancelledConference = conferenceService.addRoomToConference(id, request);

        return conferenceMapper.convertTo(cancelledConference);
    }

}