package com.conferencegateway.service;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.ConferenceRoom;
import com.conferencegateway.domain.model.Participant;
import com.conferencegateway.domain.repo.ConferenceRepository;
import com.conferencegateway.errors.InputErrorException;
import com.conferencegateway.errors.ResourceNotFoundException;
import com.conferencegateway.web.request.ConferenceRequest;
import com.conferencegateway.web.request.ConferenceRoomRequest;
import com.conferencegateway.web.request.ParticipantRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.conferencegateway.domain.enums.ConferenceStatus.ACTIVE;
import static com.conferencegateway.domain.enums.ConferenceStatus.CANCELLED;
import static com.conferencegateway.errors.ErrorMsg.*;

@Service
@Slf4j
public class ConferenceService {

    @Autowired
    private ConferenceRepository conferenceRepository;

    @Autowired
    private ParticipantService participantService;

    @Transactional
    public Conference createNewConference(ConferenceRequest request) {
        Conference newConference = Conference.builder()
                .ownerId(participantService.getExistingOrNewOwnerId(request.getOwner()))
                .title(request.getTitle())
                .date(request.getDate())
                .startTime(request.getStartTime())
                .status(ACTIVE)
                .build();
        Conference createdConference = conferenceRepository.save(newConference);
        log.info(SUCCESS + "New conference \"{}\" is created.", createdConference.getTitle());

        return createdConference;
    }

    @Transactional
    public Conference cancelConference(Long id) {
        Conference conference = getConferenceById(id);
        conference.setStatus(CANCELLED);
        return conferenceRepository.save(conference);
    }

    @Transactional
    public Conference changeConferenceOwner(Long conferenceId, ParticipantRequest request) {
        Conference conference = getConferenceById(conferenceId);
        conference.setOwnerId(participantService.getExistingOrNewOwnerId(request));
        Conference updatedConference = conferenceRepository.save(conference);
        log.info(SUCCESS + "Owner of Conference \"{}\" is updated to {}.", conference.getTitle(), request.getName());

        return updatedConference;
    }

    @Transactional
    public Conference addNewParticipantToConference(Long id, ParticipantRequest request) {
        Conference conference = getConferenceById(id);
        checkIfParticipantAlreadyRegistered(request, conference);
        Participant newParticipant = participantService.createNewParticipant(request, conference);
        log.info(SUCCESS + "New participant {} is added to conference \"{}\"", request.getName(), conference.getTitle());
        conference.getParticipants().add(newParticipant);

        return conference;
    }

    void checkIfParticipantAlreadyRegistered(ParticipantRequest request, Conference conference) {
        Optional<Participant> alreadyRegisteredParticipant = conference.getParticipants().stream()
                .filter(p -> p.getName().equals(request.getName()) && p.getBirthday().equals(request.getBirthday()))
                .findAny();
        if (alreadyRegisteredParticipant.isPresent()) {
            log.error(FAILURE + PARTICIPANT_ALREADY_REGISTERED);
            throw new InputErrorException(PARTICIPANT_ALREADY_REGISTERED);
        }
    }

    @Transactional
    public Conference getConferenceById(Long id) {
        Optional<Conference> conferenceOptional = conferenceRepository.findById(id);
        if (conferenceOptional.isEmpty()) {
            log.error(ERROR + NOT_FOUND_CONFERENCE);
            throw new ResourceNotFoundException(NOT_FOUND_CONFERENCE);
        }

        return conferenceOptional.get();
    }

    @Transactional
    public Conference updateConference(Long id, ConferenceRequest request) {
        Conference conference = getConferenceById(id);
        conference.setTitle(request.getTitle());
        conference.setDate(request.getDate());
        conference.setStartTime(request.getStartTime());
        Conference updatedConference = conferenceRepository.save(conference);
        log.info(SUCCESS + "Conference \"{}\" is updated.", conference.getTitle());

        return updatedConference;
    }

    @Transactional
    public Conference removeParticipantFromConference(Long conferenceId, Long participantId) {
        Conference conference = getConferenceById(conferenceId);
        Participant participant = participantService.getParticipant(participantId, conference);
        participantService.removeParticipant(participant);
        conference.getParticipants().remove(participant);

        return conference;
    }

    @Transactional
    public List<Conference> getConferences() {
        return conferenceRepository.findAll();
    }

    @Transactional
    public Conference addRoomToConference(Long id, ConferenceRoomRequest request) {
        Conference conference = getConferenceById(id);
        conference.setRoom(getRoomBuilder(request));

        return conferenceRepository.save(conference);
    }

    private ConferenceRoom getRoomBuilder(ConferenceRoomRequest request) {

        return ConferenceRoom.builder()
                .id(request.getId())
                .roomName(request.getRoomName())
                .location(request.getLocation())
                .roomSize(request.getRoomSize())
                .build();
    }

    @Transactional
    public List<Conference> findBookedConference(LocalDate conferenceDate) {

        return conferenceRepository.findByDate(conferenceDate);
    }

}