package com.conferencegateway.service;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.ConferenceRoom;
import com.conferencegateway.domain.repo.ConferenceRoomRepository;
import com.conferencegateway.web.request.RoomRequest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ConferenceRoomService {

    @Autowired
    private ConferenceRoomRepository conferenceRoomRepository;

    @Autowired
    private ConferenceService conferenceService;

    @Transactional
    public List<ConferenceRoom> getAvailableRooms(RoomRequest request) {
        List<Conference> bookedConferencesAtSameDate = conferenceService.findBookedConference(request.getConferenceDate());
        if (!bookedConferencesAtSameDate.isEmpty()) {
            List<Long> roomsForExclude = bookedConferencesAtSameDate.stream()
                    .map(r -> r.getRoom().getId())
                    .collect(Collectors.toList());
            return conferenceRoomRepository.findByRoomSizeGreaterThanEqualAndIdNotIn(request.getRegisteredParticipants(), roomsForExclude);
        }

        return conferenceRoomRepository.findByRoomSizeGreaterThanEqual(request.getRegisteredParticipants());
    }

}