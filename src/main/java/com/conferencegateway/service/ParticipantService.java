package com.conferencegateway.service;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.Participant;
import com.conferencegateway.domain.repo.ParticipantRepository;
import com.conferencegateway.errors.ResourceNotFoundException;
import com.conferencegateway.web.request.ParticipantRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;

import static com.conferencegateway.errors.ErrorMsg.*;

@Service
@Slf4j
public class ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    @Transactional
    public Long getExistingOrNewOwnerId(ParticipantRequest owner) {
        Optional<Participant> existingParticipant = findExistingParticipant(owner.getName(), owner.getBirthday());
        if(existingParticipant.isEmpty()) {
            return createNewOwnerId(owner);
        }

        return existingParticipant.get().getId();
    }

    @Transactional
    Long createNewOwnerId(ParticipantRequest owner) {
        Participant newParticipant = getParticipantBuilder(owner);
        Participant newOwner = participantRepository.save(newParticipant);

        return newOwner.getId();
    }

    private Optional<Participant> findExistingParticipant(String name, LocalDate birthday) {

        return participantRepository.findByNameAndBirthday(name, birthday);
    }

    @Transactional
    public Participant createNewParticipant(ParticipantRequest participant, Conference conference) {
        Participant newParticipant = getParticipantBuilder(participant);
        newParticipant.setConference(conference);
        return participantRepository.save(newParticipant);
    }

    @Transactional
    public Participant getParticipant(Long id, Conference conference) {
        Optional<Participant> participantOpt = participantRepository.findByIdAndConference(id, conference);
        if (participantOpt.isEmpty()) {
            log.error(ERROR + NOT_FOUND_PARTICIPANT);
            throw new ResourceNotFoundException(NOT_FOUND_PARTICIPANT);
        }

        return participantOpt.get();
    }

    public Participant getParticipantBuilder(ParticipantRequest request) {

        return Participant.builder()
                .name(request.getName())
                .birthday(request.getBirthday())
                .build();
    }

    @Transactional
    public void removeParticipant(Participant participant) {
        participantRepository.delete(participant);
        log.info(SUCCESS + "Participant {} is removed from Conference.", participant.getName());
    }

}