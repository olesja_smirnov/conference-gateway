package com.conferencegateway.domain.repo;

import java.time.LocalDate;
import java.util.List;

import com.conferencegateway.domain.model.Conference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {

    List<Conference> findByDate(LocalDate conferenceDate);

}