package com.conferencegateway.domain.repo;

import com.conferencegateway.domain.model.ConferenceRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, Long> {

    List<ConferenceRoom> findByRoomSizeGreaterThanEqual(Long registeredParticipants);

    List<ConferenceRoom> findByRoomSizeGreaterThanEqualAndIdNotIn(Long registeredParticipants, List<Long> roomsForExclude);

}