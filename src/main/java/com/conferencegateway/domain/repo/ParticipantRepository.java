package com.conferencegateway.domain.repo;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    Optional<Participant> findByNameAndBirthday(String name, LocalDate birthday);

    Optional<Participant> findByIdAndConference(Long id, Conference conference);

}