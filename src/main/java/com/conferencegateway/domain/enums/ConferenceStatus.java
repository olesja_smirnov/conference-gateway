package com.conferencegateway.domain.enums;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum ConferenceStatus {
    @JsonEnumDefaultValue
    ACTIVE,
    CANCELLED
}