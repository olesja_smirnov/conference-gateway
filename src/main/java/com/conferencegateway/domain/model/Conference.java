package com.conferencegateway.domain.model;

import com.conferencegateway.domain.enums.ConferenceStatus;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "conference")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = "participants")
@ToString(exclude = "participants")
@Builder
public class Conference extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    @NotNull
    private Long ownerId;

    @Column
    @NotNull
    private String title;

    @Column
    @NotNull
    private LocalDate date;

    @Column
    @NotNull
    private LocalTime startTime;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private ConferenceStatus status;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "conference_room_id", columnDefinition = "bigint",
            foreignKey = @ForeignKey(name = "conference__conference_room_id__fkey"))
    private ConferenceRoom room;

    @OneToMany(mappedBy = "conference", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<Participant> participants = new HashSet<>();

}