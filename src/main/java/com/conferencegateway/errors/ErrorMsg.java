package com.conferencegateway.errors;

public class ErrorMsg {

    public static final String ERROR = "ERROR: ";
    public static final String FAILURE = "FAILURE: ";
    public static final String NOT_FOUND_CONFERENCE = "Not found conference";
    public static final String NOT_FOUND_PARTICIPANT = "Not found participant";
    public static final String PARTICIPANT_ALREADY_REGISTERED = "Participant already registered";
    public static final String SUCCESS = "SUCCESS: ";

    private ErrorMsg() {

    }
}