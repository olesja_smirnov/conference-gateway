package com.conferencegateway.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InputErrorException extends RuntimeException {

    public InputErrorException(String message) {
        super(message);
    }

}