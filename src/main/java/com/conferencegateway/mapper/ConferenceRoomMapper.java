package com.conferencegateway.mapper;

import com.conferencegateway.domain.model.ConferenceRoom;
import com.conferencegateway.web.response.ConferenceRoomResponse;
import org.dozer.DozerConverter;
import org.springframework.stereotype.Service;

@Service
public class ConferenceRoomMapper extends DozerConverter<ConferenceRoom, ConferenceRoomResponse> {

    public ConferenceRoomMapper() {
        super(ConferenceRoom.class, ConferenceRoomResponse.class);
    }

    @Override
    public ConferenceRoomResponse convertTo(ConferenceRoom conferenceRoom, ConferenceRoomResponse conferenceRoomResponse) {

        return ConferenceRoomResponse.builder()
                .id(conferenceRoom.getId())
                .roomName(conferenceRoom.getRoomName())
                .location(conferenceRoom.getLocation())
                .roomSize(conferenceRoom.getRoomSize())
                .build();
    }

    @Override
    public ConferenceRoom convertFrom(ConferenceRoomResponse conferenceRoomResponse, ConferenceRoom conferenceRoom) {
        return null;
    }

}