package com.conferencegateway.mapper;

import com.conferencegateway.domain.model.Conference;
import com.conferencegateway.domain.model.ConferenceRoom;
import com.conferencegateway.domain.model.Participant;
import com.conferencegateway.domain.repo.ParticipantRepository;
import com.conferencegateway.web.response.ConferenceResponse;
import com.conferencegateway.web.response.ConferenceRoomResponse;
import com.conferencegateway.web.response.ParticipantResponse;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ConferenceMapper extends DozerConverter<Conference, ConferenceResponse> {

    public ConferenceMapper() {
        super(Conference.class, ConferenceResponse.class);
    }

    @Autowired
    private ParticipantMapper participantMapper;

    @Autowired
    private ConferenceRoomMapper roomMapper;

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public ConferenceResponse convertTo(Conference conference, ConferenceResponse conferenceResponse) {

        return ConferenceResponse.builder()
                .id(conference.getId())
                .owner(getOwner(conference.getOwnerId()))
                .title(conference.getTitle())
                .date(conference.getDate())
                .startTime(conference.getStartTime())
                .status(conference.getStatus().name())
                .room(getRoom(conference.getRoom()))
                .participants(getParticipants(conference.getParticipants()))
                .build();
    }

    private ConferenceRoomResponse getRoom(ConferenceRoom room) {
        if (room != null) {
            return roomMapper.convertTo(room);
        }
        return null;
    }

    private ParticipantResponse getOwner(Long ownerId) {
        Participant owner = participantRepository.getById(ownerId);

        return participantMapper.convertTo(owner);
    }

    private Set<ParticipantResponse> getParticipants(Set<Participant> participants) {

        return participants.stream()
                .map(p -> participantMapper.convertTo(p))
                .collect(Collectors.toSet());
    }

    @Override
    public Conference convertFrom(ConferenceResponse conferenceResponse, Conference conference) {
        return null;
    }

}