package com.conferencegateway.mapper;

import com.conferencegateway.domain.model.Participant;
import com.conferencegateway.web.response.ParticipantResponse;
import org.dozer.DozerConverter;
import org.springframework.stereotype.Service;

@Service
public class ParticipantMapper  extends DozerConverter<Participant, ParticipantResponse> {

    public ParticipantMapper() {
        super(Participant.class, ParticipantResponse.class);
    }

    @Override
    public ParticipantResponse convertTo(Participant participant, ParticipantResponse participantResponse) {

        return ParticipantResponse.builder()
                .id(participant.getId())
                .name(participant.getName())
                .birthday(participant.getBirthday())
                .build();
    }

    @Override
    public Participant convertFrom(ParticipantResponse participantResponse, Participant participant) {
        return null;
    }

}