--liquibase formatted sql
--changeset olesja:create-conference-room-table
CREATE TABLE if not exists conference_room
(
    id              bigint AUTO_INCREMENT PRIMARY KEY,
    --AuditModel--
    created_at      timestamp not null default CURRENT_TIMESTAMP(),
    updated_at      timestamp not null default CURRENT_TIMESTAMP(),
    --
    location        varchar(80) not null,
    room_name       varchar(64) not null,
    room_size       int(10) not null
);

INSERT into conference_room values (1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Tallinn', 'Venus', 2),
                                   (2, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Parnu', 'Mercury', 5),
                                   (3, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Tartu', 'Mars', 7),
                                   (4, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Narva', 'Jupiter', 10),
                                   (5, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Kohtla-Jarve', 'Saturn', 12),
                                   (6, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Peetri alevik', 'Neptune', 15),
                                   (7, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Viljandi', 'Pluto', 17),
                                   (8, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Rapla', 'Earth', 20),
                                   (9, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'Kihnu', 'Uranus', 100);