--liquibase formatted sql
--changeset olesja:create-conference-table
CREATE TABLE if not exists conference
(
    id                  bigint AUTO_INCREMENT PRIMARY KEY,
    --AuditModel--
    created_at          timestamp not null default CURRENT_TIMESTAMP(),
    updated_at          timestamp not null default CURRENT_TIMESTAMP(),
    --
    owner_id            varchar(64) not null,
    title               varchar(64) not null,
    date                date not null,
    start_time          time not null,
    status              varchar(64) not null,
    conference_room_id  bigint,
    CONSTRAINT conference__conference_room_id__fkey FOREIGN KEY (conference_room_id) references conference_room (id)
);