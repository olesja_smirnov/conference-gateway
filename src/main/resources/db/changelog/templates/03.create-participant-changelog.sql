--liquibase formatted sql
--changeset olesja:create-participant-table
CREATE TABLE if not exists participant
(
    id              bigint AUTO_INCREMENT PRIMARY KEY,
    --AuditModel--
    created_at      timestamp not null default CURRENT_TIMESTAMP(),
    updated_at      timestamp not null default CURRENT_TIMESTAMP(),
    --
    name            varchar(64) not null,
    birthday        date not null,
    conference_id   bigint,
    CONSTRAINT participant__conference_id__fkey FOREIGN KEY (conference_id) references conference (id)
);