FROM adoptopenjdk/openjdk13-openj9:alpine-slim
EXPOSE 8081
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} conference-gateway.jar
ENTRYPOINT ["java","-jar","/conference-gateway.jar"]