# conference-gateway
You need to run `conference-webapp` application as frontend module or to run both modules via Docker (check configuration below)

# Getting Started
Set SDK for project (JDK 13)

## IDEA configuration
Install plugin "Lombok" to your editor (for older IDEA version)

## API documentation
* Open API doc via Swagger http://localhost:8081/swagger-ui.html

## DB CONFIGURATION
After running the application pls navigate to http://localhost:8081/h2-console
   
   DriverClassName: org.h2.Driver
   JDBC URL: jdbc:h2:file:~/conference
   - UserName: username
   - Password: password
   
## Docker configuration
* For creating an image run: `docker build -t conference-gw .`
* Then run the container from the image: `docker run -it -p 8081:8081 -d conference-gw`

## Task
Konverentside halduse rakendus

Loo andmebaasiga REST veebirakendus, millega saab hallata konverentsikeskuse konverentse.

Baasfunktsionaalsus (vajalikud lisategevused lisa ise):
* kasutaja loob konverentsi;
* kasutaja tühistab konverentsi;
* kasutaja kontrollib konverentsiruumide saadavust (vastavalt registreerunud kasutajatele ja ruumi suurusele);
* kasutaja annab üle konverentsi teisele kasutajale (loovutab)
* kasutaja lisab osavõtja konverentsi;
* kasutaja eemaldab osavõtja konverentsist.

Andmemudel (baasandmed, vajalikud lisaväljad lisa ise):
* Konverents: konverentsi nimi, toimumise kuupäev ning algusaeg.
* Osaleja: nimi ja sünnipäev.
* Konverentsiruum: nimi, asukoht, mahutavus.

Rakendusele on järgnevad tehnilised nõuded:
* Back-end: Java ja Spring Boot
    * REST API struktuur vastab üldlevinud standarditele
    * Back-endile on kirjutatud vajalikud testid
* Front-end:
    * Kasutada parimaid praktikaid (nt modulaarsus)
* Andmebaas on relatsiooniline
* Rakenduse / andmebaasi käivitamiseks on eelistatud dockeri kasutamine

Peab kindlasti mõtlema ka nende olukordade peale, kui 
rakendusse (nii front-endi kui ka API-sse) ei sisestata oodatuid andmeid ning tekivad vead. 